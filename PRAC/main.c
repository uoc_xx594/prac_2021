/*
 * Copyright (C) 2017 Universitat Oberta de Catalunya - http://www.uoc.edu/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Universitat Oberta de Catalunya nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*----------------------------------------------------------------------------*/

/* Standard includes */
#include <stdlib.h>
#include <stdio.h>


/* Free-RTOS includes */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "portmacro.h"


/* MSP432 drivers includes */
#include "msp432_launchpad_board.h"
#include "BumpInt.h"
#include "driverlib.h"
#include "motor.h"
#include "uart_driver.h"
#include "encoder.h"


/*----------------------------------------------------------------------------*/

#define TASK_PRIORITY               ( tskIDLE_PRIORITY + 2 )
#define HEARTBEAT_TASK_PRIORITY     ( tskIDLE_PRIORITY + 1 )

#define TASK_STACK_SIZE             ( 1024 )
#define HEARTBEAT_STACK_SIZE        ( 128 )

#define HEART_BEAT_ON_MS            ( 10 )
#define HEART_BEAT_OFF_MS           ( 990 )
#define DEBOUNCING_MS               ( 20 )

#define QUEUE_SIZE                  ( 50 )

#define TX_UART_MESSAGE_LENGTH      ( 128 )

#define pdTICKS_TO_MS( xTimeInMs ) ( ( TickType_t ) ( ( ( TickType_t ) ( xTimeInMs ) * ( TickType_t ) 1000 ) / ( TickType_t ) configTICK_RATE_HZ ) )

/*----------------------------------------------------------------------------*/


// Tasks
static void HeartBeatTask(void *pvParameters);
static void MotorTask(void *pvParameters);
static void EncoderTask(void *pvParameters);

// callbacks & functions


//Task sync tools and variables


/*----------------------------------------------------------------------------*/


static void HeartBeatTask(void *pvParameters){
    for(;;){
        led_toggle(MSP432_LAUNCHPAD_LED_RED);
        vTaskDelay( pdMS_TO_TICKS(HEART_BEAT_ON_MS) );
        led_toggle(MSP432_LAUNCHPAD_LED_RED);
        vTaskDelay( pdMS_TO_TICKS(HEART_BEAT_OFF_MS) );
    }
}


static void MotorTask(void *pvParameters) {
    int8_t pwm = 1;
    bool direction = true;

    MotorStart(MOTOR_BOTH);

    for (;;) {

        MotorConfigure(MOTOR_RIGHT, MOTOR_DIR_FORWARD, pwm);
        MotorConfigure(MOTOR_LEFT, MOTOR_DIR_BACKWARD, pwm);


        vTaskDelay(pdMS_TO_TICKS(5000));

        if (pwm > 100 || pwm < 0) {
            direction = !direction;
        }

        if (direction) {
            pwm += 1;
        } else {
            pwm -= 1;
        }
    }

}

static void EncoderTask(void *pvParameters) {

    TickType_t previous_ticks, current_ticks, elapsed_ticks;
    uint32_t elapsed_ms;
    float left_distance_mm, left_speed_mm_s, right_distance_mm, right_speed_mm_s;
    char message[TX_UART_MESSAGE_LENGTH];

    // Wait for motors to start
    vTaskDelay(pdMS_TO_TICKS(1000));

    // Get previous ticks
    previous_ticks = xTaskGetTickCount();

    uart_print("Initiating speed measurements... \n\r");
    for (;;) {
        // Get current ticks
        current_ticks = xTaskGetTickCount();

        // Calculate elapsed milliseconds and update variables
        elapsed_ticks = current_ticks - previous_ticks;
        previous_ticks = current_ticks;
        elapsed_ms = pdTICKS_TO_MS(elapsed_ticks);

        // Calculate wheel speed for left and right motors
        EncoderGetSpeed(ENCODER_LEFT, elapsed_ms, &left_distance_mm, &left_speed_mm_s);
        EncoderGetSpeed(ENCODER_RIGHT, elapsed_ms, &right_distance_mm, &right_speed_mm_s);

        // send via UART
        sprintf(message, "Left wheel speed: %.1f, Right wheel speed: %.1f \n\r", left_speed_mm_s, right_speed_mm_s);
        uart_print(message);

        // Delay task execution
        vTaskDelay(pdMS_TO_TICKS(2000));
    }

}



/*----------------------------------------------------------------------------*/

int main(int argc, char** argv)
{
    int32_t retVal = -1;

    // Initialize semaphores, queues,...

    /* Initialize the board and peripherals */
    board_init();
    MotorInit();
    EncoderInit();
    uart_init(NULL);
    BumpInt_Init(NULL);


    if ( true ) {  //can be used to check the existence of FreeRTOS sync tools

        /* Create HeartBeat task */
        retVal = xTaskCreate(HeartBeatTask, "HeartBeatTask", HEARTBEAT_STACK_SIZE, NULL, HEARTBEAT_TASK_PRIORITY, NULL );
        if(retVal < 0) {
            led_on(MSP432_LAUNCHPAD_LED_RED);
            while(1);
        }

        /* Create Motor task */
        retVal = xTaskCreate(MotorTask, "MotorTask", TASK_STACK_SIZE, NULL, TASK_PRIORITY, NULL );
        if(retVal < 0) {
            led_on(MSP432_LAUNCHPAD_LED_RED);
            while(1);
        }

        /* Create Encoder task */
        retVal = xTaskCreate(EncoderTask, "EncoderTask", TASK_STACK_SIZE, NULL, TASK_PRIORITY, NULL );
        if(retVal < 0) {
            led_on(MSP432_LAUNCHPAD_LED_RED);
            while(1);
        }

        /* Start the task scheduler */
        vTaskStartScheduler();
    }

    return 0;
}

/*----------------------------------------------------------------------------*/
